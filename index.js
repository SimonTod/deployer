let express = require('express');
let session = require('express-session');
let bodyParser = require('body-parser');
let fs = require('fs');
let md5 = require('md5');
let Users = require('./users');
let AppSession = require('./session');
let Projects = require('./projects');
let Settings = require('./settings');

let app = express();

app.set('views', './views');
app.set('view engine', 'jade');
app.use('/scripts', express.static(__dirname + '/scripts/'));
app.use('/bootstrap', express.static(__dirname + '/node_modules/bootstrap/dist/'));
app.use('/jquery', express.static(__dirname + '/node_modules/jquery/dist/'));
app.use('/font-awesome', express.static(__dirname + '/node_modules/font-awesome/'));
app.use('/styles', express.static(__dirname + '/styles/'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
app.use(express.json());
app.use(express.urlencoded({extended: true}));
app.use(session({
    secret: 'work hard',
    resave: true,
    saveUninitialized: false
  }));

let users = new Users();
let appSession = new AppSession();
let projectsClass = new Projects();
let appSettings = new Settings();

app.get('/', function(req, res) {
    fs.readFile(__dirname + '/settings.json', "utf8", function(err, data) {
        if (err) return res.redirect('/create-admin');

        let settings = JSON.parse(data);
        if (typeof settings.projects == 'undefined') settings.projects = [];

        res.render('home', {
            settings: settings.settings,
            projects: settings.projects,
            user: req.session.user
        })
    })
})

app.get('/create-admin', users.getCreateAdmin);

app.post('/create-admin', users.postCreateAdmin);

app.get('/login', appSession.getLogin);

app.post('/login', appSession.postLogin);

app.get('/logout', appSession.logout);

app.get('/manage-projects', projectsClass.listProjects);

app.post('/add-project', projectsClass.addProject);

app.get('/remove-Project', projectsClass.removeProject);

app.get('/settings', appSettings.getSettings);

app.post('/settings', appSettings.postSettings);

app.listen(3000);
