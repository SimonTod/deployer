var fs = require('fs');

module.exports = class Projects {

    listProjects(req, res) {
        fs.readFile(__dirname + '/settings.json', "utf8", function(err, data) {
            if (err || typeof req.session.user == 'undefined' || req.session.user.type != 'Admin') return res.redirect('/');

            var settings = JSON.parse(data);
            if (typeof settings.projects == 'undefined') settings.projects = [];

            res.render('projects', {
                settings: settings.settings,
                projects: settings.projects,
                user: req.session.user
            })
        })
    }

    addProject(req, res) {
        fs.readFile(__dirname + '/settings.json', "utf8", function(err, data) {
            if (err || typeof req.session.user == 'undefined' || req.session.user.type != 'Admin') return res.redirect('/');

            var settings = JSON.parse(data);
            if (typeof settings.projects == 'undefined') settings.projects = [];

            var newProject = {
                name: req.body.name,
                url: req.body.url,
                command: req.body.command
            }

            settings.projects.push(newProject);

            fs.writeFile('settings.json', JSON.stringify(settings), {encoding:'utf8',flag:'w'}, function() {
                if (err) {console.log(err);}
                res.redirect('/manage-projects');
            })  
        })
    }

    removeProject(req, res) {
        fs.readFile(__dirname + '/settings.json', "utf8", function(err, data) {
            if (err || typeof req.session.user == 'undefined' || req.session.user.type != 'Admin') return res.redirect('/');

            var settings = JSON.parse(data);
            if (typeof settings.projects == 'undefined') settings.projects = [];

            var queryProject = {
                name: decodeURIComponent(req.query.name),
                url: decodeURIComponent(req.query.url),
                command: decodeURIComponent(req.query.command)
            }

            var toRemove = settings.projects.find(function(element) {
                return JSON.stringify(element) == JSON.stringify(queryProject);
            })

            if (toRemove) {
                settings.projects.splice(settings.projects.indexOf(toRemove), 1);
            }

            fs.writeFile('settings.json', JSON.stringify(settings), {encoding:'utf8',flag:'w'}, function() {
                if (err) {console.log(err)}
                res.redirect('/manage-projects');
            })
        })
    }

}
