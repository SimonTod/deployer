var fs = require('fs');
var md5 = require('md5');

module.exports = class Users {

    getCreateAdmin(req, res) {
        fs.readFile(__dirname + '/settings.json', "utf8", function(err, data) {
            if (!err) { return res.redirect('/'); }
    
            var content = {
                settings: {
                    websiteTitle: "Deployer",
                    websiteSubTitle: "Welcome to my deployer application"
                }
            }

            fs.writeFile('settings.json', JSON.stringify(content), function(err) {
                if (err) {console.log(err);}
                res.render('create-admin', {
                    settings: content.settings
                });
            })
            
        })
    }

    postCreateAdmin(req, res) {
        fs.readFile(__dirname + '/settings.json', "utf8", function(err, data) {
            if (data.users) return res.redirect('/');

            var settings = JSON.parse(data);
            settings.users = {
                admin: {
                    email: req.body.email,
                    password: md5(req.body.password)
                }
            }
        
            fs.writeFile('settings.json', JSON.stringify(settings), function(err) {
                if (err) {console.log(err);}
                req.session.user = {type: 'Admin'};
                return res.redirect('/settings');
            })
        })
    }

}
